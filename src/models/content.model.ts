import {Entity, model, property} from '@loopback/repository';

@model()
export class Content extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  idContent?: number;

  @property({
    type: 'string',
    description: "Type of content: video,text,audio,image",
    required: true,
  })
  type: string;

  @property({
    type: 'Object',
    required: true,
  })
  contentPost: Object;

  @property({
    type: 'number',
    required: true,
  })
  length: number;

  constructor(data?: Partial<Content>) {
    super(data);
  }
}

export interface ContentRelations {
  // describe navigational properties here
}

export type ContentWithRelations = Content & ContentRelations;
